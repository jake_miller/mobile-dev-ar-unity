﻿using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Collider))]
public class BoundaryReset : MonoBehaviour
{
    private List<Tuple<Vector3, Quaternion>> startValues = new List<Tuple<Vector3, Quaternion>>();

    void Start()
    {
        foreach (Transform t in GetComponentsInChildren<Transform>())
        {
            startValues.Add(new Tuple<Vector3, Quaternion>(t.position, t.rotation));
        }
        Debug.Log(startValues.Count);
    }

    private void OnTriggerExit(Collider _)
    {
        int i = 0;
        foreach (Transform t in GetComponentsInChildren<Transform>())
        {
            Tuple<Vector3, Quaternion> startValue = startValues[i];
            t.position = startValue.Item1;
            t.rotation = startValue.Item2;

            Rigidbody r = t.GetComponent<Rigidbody>();
            r.velocity = Vector3.zero;
            r.angularVelocity = Vector3.zero;

            i++;
        }
        Debug.Log("Reset to start");
    }

    void Update()
    {

    }
}
