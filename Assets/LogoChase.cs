﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoChase : MonoBehaviour
{
    public Transform transformToChase;

    private MeshRenderer meshRenderer;
    private Rigidbody r;


    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        r = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (meshRenderer.enabled)
        {
            Vector3 lookDirection = (transformToChase.position - transform.position).normalized;
            r.velocity = lookDirection * 1.5f;
        }
        else
        {
            r.velocity = Vector3.zero;
        }
    }
}
